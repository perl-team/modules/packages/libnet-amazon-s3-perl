Source: libnet-amazon-s3-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Damyan Ivanov <dmn@debian.org>,
           gregor herrmann <gregoa@debian.org>,
           Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libdata-stream-bulk-perl <!nocheck>,
                     libdatetime-format-http-perl <!nocheck>,
                     libdigest-hmac-perl <!nocheck>,
                     libdigest-md5-file-perl <!nocheck>,
                     libexporter-tiny-perl <!nocheck>,
                     libfile-find-rule-perl <!nocheck>,
                     libhttp-date-perl <!nocheck>,
                     libhttp-message-perl <!nocheck>,
                     liblwp-useragent-determined-perl <!nocheck>,
                     libmime-types-perl <!nocheck>,
                     libmoose-perl <!nocheck>,
                     libmoosex-role-parameterized-perl <!nocheck>,
                     libmoosex-strictconstructor-perl <!nocheck>,
                     libmoosex-types-datetime-morecoercions-perl <!nocheck>,
                     libnamespace-clean-perl <!nocheck>,
                     libpath-class-perl <!nocheck>,
                     libref-util-perl <!nocheck>,
                     libregexp-common-perl <!nocheck>,
                     libsafe-isa-perl <!nocheck>,
                     libscalar-list-utils-perl <!nocheck>,
                     libsub-override-perl <!nocheck>,
                     libterm-encoding-perl <!nocheck>,
                     libterm-progressbar-simple-perl <!nocheck>,
                     libtest-deep-perl <!nocheck>,
                     libtest-exception-perl <!nocheck>,
                     libtest-lwp-useragent-perl <!nocheck>,
                     libtest-mocktime-perl <!nocheck>,
                     libtest-warnings-perl <!nocheck>,
                     liburi-perl <!nocheck>,
                     libvm-ec2-security-credentialcache-perl <!nocheck>,
                     libwww-perl <!nocheck>,
                     libxml-libxml-perl <!nocheck>,
                     perl
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libnet-amazon-s3-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libnet-amazon-s3-perl.git
Homepage: https://metacpan.org/release/Net-Amazon-S3
Rules-Requires-Root: no

Package: libnet-amazon-s3-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libdata-stream-bulk-perl,
         libdatetime-format-http-perl,
         libdigest-hmac-perl,
         libdigest-md5-file-perl,
         libfile-find-rule-perl,
         libhttp-date-perl,
         libhttp-message-perl,
         liblwp-useragent-determined-perl,
         libmime-types-perl,
         libmoose-perl,
         libmoosex-role-parameterized-perl,
         libmoosex-strictconstructor-perl,
         libmoosex-types-datetime-morecoercions-perl,
         libnamespace-clean-perl,
         libpath-class-perl,
         libref-util-perl,
         libregexp-common-perl,
         libsafe-isa-perl,
         libscalar-list-utils-perl,
         libsub-override-perl,
         libterm-encoding-perl,
         libterm-progressbar-simple-perl,
         liburi-perl,
         libwww-perl,
         libxml-libxml-perl
Recommends: libvm-ec2-security-credentialcache-perl
Description: Amazon S3 Perl API - Simple Storage Service
 Net::Amazon::S3 provides a Perlish interface to Amazon S3. From the
 developer blurb: "Amazon S3 is storage for the Internet. It is
 designed to make web-scale computing easier for developers. Amazon S3
 provides a simple web services interface that can be used to store and
 retrieve any amount of data, at any time, from anywhere on the web. It
 gives any developer access to the same highly scalable, reliable,
 fast, inexpensive data storage infrastructure that Amazon uses to run
 its own global network of web sites. The service aims to maximize
 benefits of scale and to pass those benefits on to developers".
 .
 To find out more about S3, please visit: http://s3.amazonaws.com/
 .
 To use this module you will need to sign up to Amazon Web Services and
 provide an "Access Key ID" and " Secret Access Key". If you use this
 module, you will incur costs as specified by Amazon. Please check the
 costs. If you use this module with your Access Key ID and Secret
 Access Key you must be responsible for these costs.
