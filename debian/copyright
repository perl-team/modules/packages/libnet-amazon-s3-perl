Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/Net-Amazon-S3
Upstream-Contact: Leo Lapworth <llap@cpan.org>
Upstream-Name: Net-Amazon-S3

Files: *
Copyright: 2022, Rusty Conover <rusty@luckydinosaur.com>
           2022, Pedro Figueiredo <me@pedrofigueiredo.org>
           2022, Brad Fitzpatrick <brad@danga.com>
           2022, Leon Brocard <acme@astray.com>
           2022, Amazon Digital Services programmers
           2022, Branislav Zahradník
License: other
 This is published under the terms of the following Amazon licence:
 .
 This software code is made available "AS IS" without warranties of any
 kind.  You may copy, display, modify and redistribute the software
 code either by itself or as incorporated into your code; provided that
 you do not remove any proprietary notices.  Your use of this software
 code is at your own risk and you waive any claim against Amazon
 Digital Services, Inc. or its affiliates with respect to your use of
 this software code. (c) 2006 Amazon Digital Services, Inc. or its
 affiliates.

Files: debian/*
Copyright: 2006, Alexis Sukrieh <sukria@debian.org>
 2007, 2008, Damyan Ivanov <dmn@debian.org>
 2007-2022, gregor herrmann <gregoa@debian.org>
 2008, Joey Hess <joeyh@debian.org>
 2008, Roberto C. Sanchez <roberto@debian.org>
 2009, Ryan Niebur <ryan@debian.org>
 2011, Fabrizio Regalli <fabreg@fabreg.it>
 2013-2021, Florian Schlichting <fsfs@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
